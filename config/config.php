<?php
/**
 * Created by king in the north.
 * User: 张慧君
 * Date: 2019/4/30
 * Time: 1:56 PM
 * Say : Remember why you started!
 * websocket服务配置,以及redis配置
 */

define('WEBSOCKET_IP','0.0.0.0');
define('WEBSOCKET_PORT',9500);

define('HEARTBEAT_IDLE_TIME',120);
define('HEARTBEAT_CHECK_INTERVAL',60);

define('REDIS_IP','127.0.0.1');
define('REDIS_PORT',6379);
define('REDIS_PASSWORD','zhj');
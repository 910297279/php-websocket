# php-websocket

#### 介绍
 使用swoole的websocket服务来实现消息的推送,可群发,也可单点推送,实时查看当前websocket在线客户端的连接数

#### 依赖
1. 需要安装swoole扩展,swoole扩展安装请移步https://wiki.swoole.com/wiki/page/6.html
2. 需要安装redis任意版本,redis安装自行百度

#### 使用教程

1. 现在你已经clone到了本地,之后直接启动服务端脚本,默认端口为9500,cli执行: php websocket.php ,开启websocket服务
2. 现在你已经开起了websocket服务,之后连接到websocket服务端,我使用在线websocket测试工具来做为websocket客户端(http://coolaf.com/tool/chattest),实际项目中前端编写js脚本来编写websocket客户端
3. 客户端连接上之后,发送{"identity":"zhj"}这条信息,来告诉服务端,客户端的名称,这时候redis中会保存当前客户端的标识,前面是swoole生成的对应该客户端的唯一身份id,后面名称是我们自定义的该客户端的名称,将他俩绑定起来
4. 现在你可以http协议来获取当前所有在线的客户端,只要遍历redis中的liveset集合即可,如果websocket客户端主动或者被动断开连接的话,liveset集合中对应的数据会被清除
5. 之后使用浏览器,输入地址 http://127.0.0.1:9500/?message=hellow world&choose=zhj  ,message:为你要发的内容,choose:为你要发给哪个websocket客户端,回车,即可发送消息到你想要发送给的websocket客户端

#### 使用说明

1. 直接clone到你的任意文件夹下面,之后需要你的redis服务可以正常使用,将redis的密码修改成你自己的redis密码即可,客户端如果超过2分钟没有向服务端发送消息,服务端这边会主动断开连接,如果不希望有此检测,将下面的heartbeat_idle_time,heartbeat_check_interval这两个参数去掉,这个设置只要是用来防止客户端意外掉线,服务端不知道的情况

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)